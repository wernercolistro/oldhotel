package Main;

import Hotel.*;
import Huesped.*;

import java.time.LocalDate;
import java.util.Scanner;

public class Menu {
    private static Scanner sc = new Scanner(System.in);

    public void desplegarMenu(){
        System.out.println("Bienvenidos al Hotel Endava: \n" +
                "1 - Registrar reserva.\n2 - Consultar reserva por id reserva. \n" +
                "3 - Historial de reserva por habitación.\n4 - Desplegar reservas de todas las habitaciones. \n" +
                "5 - Listado de clientes\n" + "6 - Cancelar reserva.\n7 - Estado habitaciones entre fechas determinadas.\n" +
                "8 - Reservas según fechas.\n9 - Salir. ");
    }
    public void accionMenu(int opc, Bookings bookings, Customers customers, Rooms rooms){
        int numReserva, numRoom;
        int [] numFecha = new int[3];
        switch(opc){
            case 1:
                    opcNewBooking(bookings,customers,rooms);
                break;
            case 2:
                    System.out.println("Ingrese el numero de reserva: ");
                    numReserva = sc.nextInt();
                    opcConsultarReserva(bookings, numReserva);
                break;
            case 3:
                    System.out.println("Ingrese el numero de habitación: ");
                    numRoom = sc.nextInt();
                    opcListadoReservaHabitacion(bookings, numRoom);
                break;
            case 4:
                    System.out.print("\033[H\033[2J");
                    System.out.flush();
                    System.out.println(rooms.toString());
                    break;
            case 5:
                    System.out.println(customers.toString());
                    break;
            case 6:
                    System.out.println("Ingrese el numero de reserva a cancelar");
                    numReserva = sc.nextInt();
                    opcCancelarReserva(rooms,bookings,numReserva);
                break;
            case 7:
                    System.out.println("ingrese la primer fecha: yyyy mm dd ");
                    numFecha = Outputs.imprimirSolicitarFecha();
                    LocalDate date1,date2;
                    date1 = LocalDate.of(numFecha[0],numFecha[1],numFecha[2]);
                    System.out.println("ingrese la primer segunda fecha: yyyy mm dd ");
                    numFecha = Outputs.imprimirSolicitarFecha();
                    date2 = LocalDate.of(numFecha[0],numFecha[1],numFecha[2]);
                    opcEstadoHabitaciones(rooms, date1,date2);
                break;
            default:
                System.out.println("No ha ingresado una opcion valida.");
                break;
        }
    }
    //No NULL Customers// Rooms
    public void opcNewBooking(Bookings bookings, Customers customers, Rooms rooms) {
        int [] numFecha = new int[3];
        Booking book = new Booking();

        int roomNum;
        while (true) {
            System.out.println("Número de habitación");
            roomNum = sc.nextInt();
            if (roomNum > Rooms.cantRooms) {
                System.out.println("No existe habitación");
            }else{
                break;
            }
        }
        do {
            while (true) {
                try {
                    System.out.println("Fecha de Ingreso: yyyy mm dd ");
                    numFecha = Outputs.imprimirSolicitarFecha(); //Ingresa year month day en un arreglo de enteros
                    book.setCheckIn(numFecha[0], numFecha[1], numFecha[2]);
                    LocalDate currentDate = LocalDate.now();
                    if (book.getCheckIn().isBefore(currentDate)) {
                        System.out.println("No es posible registrar una fecha anterior a la fecha actual.");
                        continue;
                    }
                    break;
                } catch (Exception DateTimeException) {
                    System.out.println("No se ha ingresado una fecha correcta.");
                }
            }

            while (true) {
                try {
                    System.out.println("Fecha de Egreso: yyyy mm dd ");
                    numFecha = Outputs.imprimirSolicitarFecha();
                    book.setCheckOut(numFecha[0], numFecha[1], numFecha[2]);
                    //Verifica que la fecha de egreso sea posterior a la fecha de ingreso
                    while (book.getCheckIn().isAfter(book.getCheckOut())) {
                        System.out.println("La fecha ingresada debe ser posterior a la fecha del checkin.\n" +
                                "Ingrese nuevamente la fecha.");
                        System.out.println("Fecha de Egreso: yyyy mm dd ");
                        numFecha = Outputs.imprimirSolicitarFecha();
                        book.setCheckOut(numFecha[0], numFecha[1], numFecha[2]);
                    }
                    break;
                } catch (Exception DateTimeException) {
                    System.out.println("No se ha ingresado una fecha correcta.");
                }
            }
            if (rooms.getHashBookings().get(roomNum).hayReservaBooking(book.getCheckIn(), book.getCheckOut())) {
                System.out.println("La habitación ya esta reservada.");
            } else {
                break;
            }
        } while (true);
        book.setRoomBooking(rooms.getRoom(roomNum));
        System.out.println("Numero de documento: ");
        long id = sc.nextLong();
        if (customers.existeCustomerPorID(id)){    //busca el cliente, sino existe lo crea.
            book.setCustomerBooking(customers.devolverCustumerPorID(id));
        }else{
            System.out.println("Cliente no existe, registrar cliente: ");
            Customer customer = new Customer(id);
            customer.newCustomer();
            book.setCustomerBooking(customer);
            customers.insertCustomer(customer);
        }
        bookings.insertBooking(book);
        rooms.addBookingHashbookings(roomNum,book);

        System.out.println("El precio de la estadia sera de: U$S" + book.priceCalculator() + ".");
    }

    public void opcConsultarReserva(Bookings bookings, int numReserva){
        try {
            System.out.println(bookings.devolverBookBooking(numReserva));
        }catch (Exception IndexOutOfBoundsException){
            System.out.println("No existe la reserva en el sistema.");
        }
    }

    public void opcListadoReservaHabitacion(Bookings bookings,int numHab){
        try {
            bookings.historialBooking(numHab);
        }catch(Exception e){
            System.out.println("La habitación no dispone reservas en su historial.");
        }
    }

    public void opcCancelarReserva(Rooms rooms, Bookings bookings, int numReserva){
        int numRoom = bookings.devolverNumCuartoNumReserva(numReserva);
        Booking book = bookings.devolverBookBooking(numReserva);
        if (book.betweenCheckinCheckOut(LocalDate.now())){
            System.out.println("No es posible cancelar la reserva, la misma esta en curso");
        }else{
            rooms.getHashBookings().get(numRoom-1).removeBooking(numReserva);
        }
    }

    public void opcEstadoHabitaciones(Rooms rooms, LocalDate date1, LocalDate date2){
        for (int i = 1; i <= Rooms.cantRooms;i++){
            System.out.println(rooms.getHashRooms().get(i).toString());
            if (!rooms.getHashBookings().get(i).hayReservaBooking(date1,date2)){
                System.out.println("\t\tHabitación Disponible");
            }else{
                rooms.getHashBookings().get(i).bookingInDates(date1,date2).toString();
            }
        }
    }
}
