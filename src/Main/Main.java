package Main;
import Hotel.*;
import Huesped.*;

import java.util.Scanner;


public class Main {

    private static Scanner sc = new Scanner(System.in);
    private static boolean exit = false;
    private static final int EXIT = 9;
    public static void main(String[] args) {

        Rooms rooms = new Rooms();
        rooms.roomsSet();
        Bookings bookings = new Bookings();
        Customers customers = new Customers();
        Menu menu = new Menu();

        while (true) {

            //Inicio Test
            Customer cust1 = new Customer(1,"099111111","Cust1 Customer");
            Customer cust2 = new Customer(2,"099222222","Cust2 Testing");
            Customer cust3 = new Customer(3,"099333333","Cust3");
            Customer cust4 = new Customer(4,"099444444","Cust4");
            customers.insertCustomer(cust1);
            customers.insertCustomer(cust2);
            customers.insertCustomer(cust3);
            customers.insertCustomer(cust4);

            Booking book1 = new Booking();
            Booking book2 = new Booking();
            Booking book3 = new Booking();
            Booking book4 = new Booking();
            book1.setCheckIn(2020,01,10);
            book1.setCheckOut(2020,01,15);
            book2.setCheckIn(2021,10,30);
            book2.setCheckOut(2021,11,16);
            book3.setCheckIn(2021,10,20);
            book3.setCheckOut(2021,11,17);
            book4.setCheckIn(2021,10,20);
            book4.setCheckOut(2021,11,18);

            book1.setCustomerBooking(cust1);
            book1.setRoomBooking(rooms.getHashRooms().get(1));
            bookings.insertBooking(book1);
            rooms.getHashBookings().get(1).insertBooking(book1);

            book2.setCustomerBooking(cust2);
            book2.setRoomBooking(rooms.getHashRooms().get(5));
            bookings.insertBooking(book2);
            rooms.getHashBookings().get(5).insertBooking(book2);

            book3.setCustomerBooking(cust3);
            book3.setRoomBooking(rooms.getHashRooms().get(10));
            bookings.insertBooking(book3);
            rooms.getHashBookings().get(10).insertBooking(book3);

            book4.setCustomerBooking(cust4);
            book4.setRoomBooking(rooms.getHashRooms().get(18));
            bookings.insertBooking(book4);
            rooms.getHashBookings().get(18).insertBooking(book4);

            menu.desplegarMenu();
            System.out.println("Seleccione la opcion que desea:");
            int opc = sc.nextInt();
            //verificar if ternario
            if (opc == EXIT) {
                System.out.println("Hasta luego.!");
                break;
            } else {
                menu.accionMenu(opc, bookings, customers, rooms);
            }
        }
    }
}
