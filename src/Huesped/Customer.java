package Huesped;


import java.util.Scanner;

public class Customer implements Comparable<Customer>{
    private static Scanner sc = new Scanner(System.in);
    private long customerId;
    private String customerPhone;
    private String customerFullName;

    public Customer(){}

    public Customer(long customerId){
        this.customerId = customerId;
    }

    public Customer(long customerId, String customerPhone, String customerFullName){
        this.customerId = customerId;
        this.customerPhone = customerPhone;
        this.customerFullName = customerFullName;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerFullName() {
        return customerFullName;
    }

    public void setCustomerFullName(String customerFullName) {
        this.customerFullName = customerFullName;
    }

    public void newCustomer(){
        if (customerId == 0) {
            System.out.println("Numero de documento: ");
            long numDoc = sc.nextLong();
            this.customerId = numDoc;
        }
        System.out.println("Numero de telefono: ");
        String phone = sc.next();
        this.customerPhone = phone;
        System.out.println("Nombre Completo: ");
        String name = sc.next();
        this.customerFullName = name;
    }

    @Override
    public int compareTo(Customer customer){
        return this.getCustomerFullName().compareTo(customer.getCustomerFullName());
    }


    @Override
    public String toString(){
        return "Id: " + this.customerId + " | Telefono: " + this.customerPhone + " | Nombre: " + this.customerFullName;
    }
}

