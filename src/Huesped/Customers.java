package Huesped;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;


public class Customers {
    private ArrayList<Customer> customersList;

    public Customers() {
        customersList = new ArrayList<Customer>();
    }

    public static void main(String[] args) {
        Customers allCustomers = new Customers();
    }

    public ArrayList<Customer> getCustomersList() {
        return customersList;
    }

    public void setCustomersList(ArrayList<Customer> customersList) {
        this.customersList = customersList;
    }

    public void insertCustomer(Customer client) {
        this.customersList.add(client);
    }

    public boolean existeCustomerPorID(long id){
        boolean esta = false;
        int i = 0;
        while (!esta && i<customersList.size()){
            if (customersList.get(i).getCustomerId() == id){
                esta = true;
            }else{
                i++;
            }
        }
        return esta;
    }


    public Customer devolverCustumerPorID(long id){
        Iterator<Customer> iterator = customersList.iterator();
        while (iterator.hasNext()) {
            Customer customer = iterator.next();
            if (customer.getCustomerId() == id) {
                return customer;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        Collections.sort(this.customersList);
        String allCustomers = "";
        for (int i = 0; i < customersList.size(); i++) {
            allCustomers += customersList.get(i).toString() + "\n";
        }
        return allCustomers;
    }
}



