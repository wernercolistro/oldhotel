package Hotel;
import java.util.HashMap;
public class Rooms {
    private HashMap<Integer, Room> hashRooms;
    private HashMap<Integer, Bookings> hashBookings;
    public static final int cantRooms = 20;
    
    public Rooms(){
        this.hashRooms = new HashMap<Integer,Room>();
        this.hashBookings = new HashMap<Integer, Bookings>();
    }

    //Inicia las habitaciones, las setea como standard o luxary (Si es multiple de 5)
    public void roomsSet(){
        for (int i=1;i<=cantRooms;i++){
            this.hashRooms.put(i,new Room(i));
            this.hashBookings.put(i, new Bookings());
        }
    }

    public HashMap<Integer, Room> getHashRooms() {
        return hashRooms;
    }

    public void setHashRooms(HashMap<Integer, Room> hashRooms) {
        this.hashRooms = hashRooms;
    }

    public Room getRoom(int roomNum){
        return hashRooms.get(roomNum);
    }

    public void setHashBookings(HashMap<Integer, Bookings> hashBookings) {
        this.hashBookings = hashBookings;
    }

    public HashMap<Integer, Bookings> getHashBookings() {
        return hashBookings;
    }

    public void addBookingHashbookings(int numRoom, Booking book){
        this.hashBookings.get(numRoom).insertBooking(book);
    }

    //Imprime las habitaciones
    @Override
    public String toString(){
        String allRooms = "";
        for (int i = 1; i <= cantRooms; i++){
            allRooms += hashRooms.get(i).toString() + "\n" + hashBookings.get(i).toString() + "\n";
        }
        return allRooms;
    }
}
