package Hotel;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

public class Bookings {
    private ArrayList<Booking> bookingList;
    private Scanner sc = new Scanner(System.in);

    public Bookings(){
        bookingList = new ArrayList<Booking>();
    }

    public static void main(String[] args) {
        Bookings bookings = new Bookings();
    }

    public ArrayList<Booking> getBookingList() {
        return bookingList;
    }

    public void setBookingList(ArrayList<Booking> bookingList) {
        this.bookingList = bookingList;
    }

    public void insertBooking(Booking booking){
        this.bookingList.add(booking);
    }

    //devuelve el booking según el número de reserva
    public Booking devolverBookBooking(int bookId){
        return this.bookingList.get(bookId-1);
    }

    //Me devuelve el numero de un cuarto a partir del numero de reserva.
    public int devolverNumCuartoNumReserva(int bookId){
        return devolverBookBooking(bookId).getRoomBooking().getRoomNumber();
    }

    public void removeBooking(int bookId){
        for (int i=0; i<= this.bookingList.size(); i++){
            try {
                if (this.bookingList.get(i).getIdBooking() == bookId) {
                    this.bookingList.remove(i);
                    break;
                }
            }catch (Exception IndexOutOfBoundsException){
                continue;
            }
        }
    }

    //Imprime en pantalla todos los booking de una habitacion dada
    public void historialBooking(int numRoom){
        String allBooking = "";
        for (int i = 0; i < this.bookingList.size(); i++) {
            if (bookingList.get(i).getRoomBooking().getRoomNumber() == numRoom) {
                allBooking += this.bookingList.get(i).toString() + "\n\t\t";
            }

        }
        System.out.println(allBooking);
    }

    //imprime las reservas de una habitacion dada.
    public void reservasHabitacion(int numHab){
        for (int i = 0; i <= this.bookingList.size(); i++) {
            if (this.bookingList.get(i).getRoomBooking().getRoomNumber() == numHab) {
                System.out.println(this.bookingList.get(i).toString() + "\n");
            }
        }
    }

    //Si hay alguna reserva activa en esa fecha
    public boolean hayReservaBooking(LocalDate date1,LocalDate date2){
        boolean hayReserva = false;
        for (int i=0; i<= this.bookingList.size();i++){
            try {
                if (this.bookingList.get(i).betweenCheckinCheckOut(date1) || (this.bookingList.get(i).betweenCheckinCheckOut(date1))) {
                    hayReserva = true;
                    break;
                }
            }catch (Exception IndexOutOfBoundsException){
                return hayReserva;
            }
        }
        return hayReserva;
    }

    //Precondicion hay almenos una reserva - Devuelve arreglo con la reservas activas
    public Bookings bookingInDates(LocalDate date1, LocalDate date2){
        Bookings bookings = new Bookings();
        for(int i=0; i<= this.bookingList.size();i++){
            if (this.bookingList.get(i).betweenCheckinCheckOut(date1) || this.bookingList.get(i).betweenCheckinCheckOut(date2) ){
                bookings.bookingList.add(this.bookingList.get(i));
            }
        }
        return bookings;
    }

    @Override
    public String toString(){
        String allBooking = "";
        for (int i = 0; i < this.bookingList.size(); i++){
            allBooking += "\t\t" + this.bookingList.get(i).toString() + "\n";
        }
        if (this.bookingList.size()== 0){ allBooking = "\t\tNo hay reservas.\n";}
        return allBooking;
    }

}

