package Hotel;

import Huesped.Customer;

import java.time.LocalDate;

import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class Booking {

    private int idBooking;
    private Room roomBooking;
    private Customer customerBooking;
    private LocalDate checkIn;
    private LocalDate checkOut;
    private static int contadorBooking;

    private Scanner sc = new Scanner(System.in);



    public Booking(){
        this.idBooking = ++Booking.contadorBooking;
    }

    public Room getRoomBooking(){
        return roomBooking;
    }

    public void setRoomBooking(Room room){
        this.roomBooking = room;
    }

    public Customer getCustomerBooking() {
        return customerBooking;
    }

    public void setCustomerBooking(Customer customer) {
        this.customerBooking = customer;
    }

    public void setCheckIn(int year, int month, int day){this.checkIn = LocalDate.of(year, month, day);}

    public LocalDate getCheckIn() {
        return checkIn;
    }

    public void setCheckOut(int year, int month, int day){
        this.checkOut = LocalDate.of(year, month, day);
    }

    public LocalDate getCheckOut() {
        return checkOut;
    }

    public  int getIdBooking() {
        return idBooking;
    }

    public void setIdBooking(int idBooking) {
        this.idBooking = idBooking;
    }

    public long  bookingDayDifference() {

        long  p2 = ChronoUnit.DAYS.between(this.checkIn, this.checkOut);
        if (p2 == 0){p2 = 1;}
        return p2;
    }

    public boolean betweenCheckinCheckOut(LocalDate date){
        return (date.isAfter(this.checkIn) && date.isBefore(this.checkOut));
    }

    public double priceCalculator(){
        double finalPrice;
        long daysBetween = bookingDayDifference();
        if (this.roomBooking.getRoomType() == Room.Category.LUXURY){
            finalPrice = Room.luxuryDayPrice*daysBetween;
            if (bookingDayDifference() > 7){
                finalPrice = (finalPrice*0.90);
            }
        }else {
            finalPrice = Room.standardDayPrice * daysBetween;
            if (daysBetween > 7) {
                finalPrice = (finalPrice * 0.95);
            }
        }
        return finalPrice;
    }

    @Override
    public String toString(){
        return "Numero de Reserva: " + this.idBooking + " | Titular Reserva: " +
                this. customerBooking.getCustomerFullName() +" | Numero de habitacion: " +
                this.roomBooking.getRoomNumber() +" | Fecha de ingreso: " + this.checkIn +
                " | Fecha de egreso:" + this.checkOut + " | Cantidad de días: " + bookingDayDifference();
    }
}
