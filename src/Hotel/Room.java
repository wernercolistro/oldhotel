package Hotel;

public class Room {

    public static final int luxuryDayPrice = 90;
    public static final int standardDayPrice = 50;

    enum Category{
        LUXURY,
        STANDARD,
    }
    private Category roomType;
    private boolean minibar;
    private boolean safeBox;
    private boolean wifi = true;
    private int roomNumber;
    private boolean availableRoom = false;
    private Bookings listBookings;


    public Room (int roomNumber){
        this.roomNumber = roomNumber;
        if (roomNumber % 5 == 0){
            roomType = Category.LUXURY;
            minibar = true;
            safeBox = true;
        }else{
            roomType = Category.STANDARD;
        }
    }

    public Category getRoomType() {
        return roomType;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public boolean isMinibar() {
        return minibar;
    }

    public void setMinibar(boolean minibar) {
        this.minibar = minibar;
    }

    public boolean isSafeBox() {
        return safeBox;
    }

    public void setSafeBox(boolean safeBox) {
        this.safeBox = safeBox;
    }

    public boolean isWifi() {
        return wifi;
    }

    public void setWifi(boolean wifi) {
        this.wifi = wifi;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomType(Category roomType) {
        this.roomType = roomType;
    }

    public boolean isAvailableRoom(){
        return availableRoom;
    }

    public void setAvailableRoom(boolean availableRoom) {
        this.availableRoom = availableRoom;
    }

    public Bookings getListBookings() {
        return listBookings;
    }

    public void setListBookings(Bookings listBookings) {
        this.listBookings = listBookings;
    }

    @Override
    public String toString(){
        return "Room number: " + this.roomNumber + " | kind of room: " + this.roomType +
        " | Wifi: " + this.wifi + " | Safe box: " + this.safeBox + " |Minibar: " + this.minibar;
    }

}
